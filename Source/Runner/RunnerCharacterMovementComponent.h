// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "RunnerCharacterMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class RUNNER_API URunnerCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()
public:
		URunnerCharacterMovementComponent();
public:
	virtual void UpdateBasedMovement(float DeltaSeconds) override;
};

